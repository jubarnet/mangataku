(function () {
   "use strict";
}());
/* Controllers */

function MainCtrl($scope, Page) {
    $scope.Page = Page;
}
MainCtrl.$inject = ['$scope', 'Page'];

function HomeCtrl($scope, Page) {
    Page.setTitle('MangaTaku | Read Manga, Add Favorites, Bookmark Pages, and More...');
}
HomeCtrl.$inject = ['$scope', 'Page'];

function DocumentCtrl($scope, $location) {
    /** google analytics workaround for angular */
    function MyCtrl($scope, $location, $window) {
        $scope.$on('$viewContentLoaded', function(event) {
            $window._gaq.push(['_trackPageview', $location.path()]);
        });
    }
    $scope.changeImage = function(which) {};

    $scope.isActive = function(route) {
        return route === $location.path();
    };
    $scope.isMangaChapters = function(route) {
        return route === "/" + $location.path().split("/")[1];
    };
}
DocumentCtrl.$inject = ['$scope', '$location'];

function LegalCtrl($scope, Page) {
    Page.setTitle('Legal | MangaTaku | Read Manga, Add Favorites, Bookmark Pages, and More...');
}
LegalCtrl.$inject = ['$scope', 'Page'];

function SearchCtrl($scope, $routeParams, Page) {
    function decodeParam(param) {
        return decodeURIComponent(decodeURIComponent(param));
    }

    function getMangaList() {
        return $scope.mangaList;
    }

    $scope.query = decodeParam($routeParams.query);
    Page.setTitle('Search Results for: ' + $scope.query + ' | MangaTaku | Read Manga, Add Favorites, Bookmark Pages, and More...');
}
SearchCtrl.$inject = ['$scope', '$routeParams', 'Page'];

function MangaListCtrl($scope, $location, $routeParams, MangaEden, LocalStorage, TimeUtil, Page) {
  
    /** scope variables */
    $scope.mangaList = {};
    Page.setTitle('MangaTaku | Random site I made to learn AngularJS/Bootstrap');

    /** scope functions */
    $scope.search = function(query) {
        $location.path('/search/' + query);
    };
    $scope.encodeParam = function(param) {
        return encodeURIComponent(encodeURIComponent(param));
    };
    $scope.getMangaImageUrl = function(param) {
        return MangaEden.getBaseImageUrl() + param;
    };
    $scope.searchButton = function() {
        $location.path('/search/' + jQuery(".search-query").val());
    };
    /** helper functions */

    function decodeParam(param) {
        return decodeURIComponent(decodeURIComponent(param));
    }
    /** controller logic */

    //check if a manga list exists in local storage
    try {
        LocalStorage.getMangaListLocalStorage().exists(LocalStorage.getLocalStorageKeys().MangaList, function(mangaListExists) {
            if (mangaListExists) {
                //set the mangaList variable to the value in local storage
                LocalStorage.getMangaListLocalStorage().get(LocalStorage.getLocalStorageKeys().MangaList, function(localStorageData) {
                    //before setting the mangaList value we check to see if it is valid JSON
                    try {
                        $scope.mangaList = JSON.parse(localStorageData.value);
                        //check if the manga list has expired in local storage
                        if (TimeUtil.hasExpiredWithinOneWeek(localStorageData.lastUpdated)) {
                            /** temp code for just local manga list */
                            MangaEden.getMangaList("local").then(function(fs) {
                                try {
                                    //we set the scope mangaList to the filesystems copy of manga list while we fetch from remote, so  the user does not have to wait
                                    $scope.mangaList = fs;
                                    //save the manga list to local storage
                                    LocalStorage.saveMangaListToLocalStorage(fs);
                                }
                                catch (e) {
                                    console.log(e);
                                }
                            });
                            //it has expired, fetch a new manga list from remote
                            MangaEden.getMangaList("remote").then(function(data) {
                                //if faulty data is not returned, go ahead and store it in local storage, otherwise just load LS data
                                if (data) {
                                    $scope.mangaList = data;
                                    //save the manga list to local storage
                                    LocalStorage.saveMangaListToLocalStorage(data);
                                }
                            });
                        }
                    }
                    catch (e) {
                        //console.log(e);
                        //we need to fix the manga list in local storage. Use filesystem's manga list and call remote
                        MangaEden.getMangaList("local").then(function(fs) {
                            try {
                                //we set the scope mangaList to the filesystems copy of manga list while we fetch from remote, so  the user does not have to wait
                                $scope.mangaList = fs;
                                //save the manga list to local storage
                                LocalStorage.saveMangaListToLocalStorage(fs);
                                //fetch a new manga list from remote
                                MangaEden.getMangaList("remote").then(function(data) {
                                    //if faulty data is not returned, go ahead and store it in local storage, otherwise just load LS data
                                    if (data) {
                                        $scope.mangaList = data;
                                        //save the manga list to local storage
                                        LocalStorage.saveMangaListToLocalStorage(data);
                                    }
                                });
                            }
                            catch (e) {
                                //console.log(e);
                            }
                        });
                    }
                });
            }
            //manga list does not exist in local storage, so we fetch one from remote
            else {
                //since we are making a remote call, we set the manga list scope initially to the list in file system, so the user does not have to wait to see a list
                MangaEden.getMangaList("local").then(function(fs) {
                    try {
                        //we set the scope mangaList to the filesystems copy of manga list while we fetch from remote, so  the user does not have to wait
                        $scope.mangaList = fs;
                        //fetch a new manga list from remote
                        MangaEden.getMangaList("remote").then(function(data) {
                            //if faulty data is not returned, go ahead and store it in local storage, otherwise just load LS data
                            if (data) {
                                $scope.mangaList = data;
                                //save the manga list to local storage
                                LocalStorage.saveMangaListToLocalStorage(data);
                            }
                        });
                    }
                    catch (e) {
                        //console.log(e);
                    }
                });
            }
        });
    }
    catch (error) {
        //console.log(error);
        //since we are making a remote call, we set the manga list scope initially to the list in file system, so the user does not have to wait to see a list
        MangaEden.getMangaList("local").then(function(fs) {
            try {
                //we set the scope mangaList to the filesystems copy of manga list while we fetch from remote, so  the user does not have to wait
                $scope.mangaList = fs;
                //save the manga list to local storage
                LocalStorage.saveMangaListToLocalStorage(fs);
            }
            catch (e) {
                //console.log(e);
            }
        });
    }
    
}
MangaListCtrl.$inject = ['$scope', '$location', '$routeParams', 'MangaEden', 'LocalStorage', 'TimeUtil', 'Page'];


function MangaDetailsCtrl($scope, $routeParams, $location, MangaEden, LocalStorage, TimeUtil, Favorite, $timeout, Page) {
    /** scope variables */
    $scope.mangaId = decodeParam($routeParams.mangaId);
    $scope.mangaImage = MangaEden.getBaseImageUrl() + decodeParam($routeParams.mangaImage);
    $scope.showDetails = false;
    $scope.showAddAlert = false;
    $scope.showAddAlertError = false;

    /** scope functions */
    $scope.encodeParam = function(param) {
        return encodeURIComponent(encodeURIComponent(param));
    };
    $scope.decodeHtml = function(param) {
        return $('<div/>').html(param).text();
    };
    $scope.formatCategories = function(categories) {
        if (!categories) {
            return "";
        }
        var catStr = "";
        for (var i = 0; i < categories.length; i++) {
            catStr += categories[i] + ", ";
        }
        return catStr.substring(0, catStr.length - 2);
    };
    $scope.translateStatus = function(status) {
        if (status === 2) {
            return "Complete";
        }
        else if (status === 1) {
            return "Ongoing";
        }
        else {
            return "Stopped";
        }
    };
    function decodeParam(param) {
        return decodeURIComponent(decodeURIComponent(param));
    }
    $scope.getFirstChapter = function(chapters) {
        return chapters.reverse()[0];
    };
    $scope.addFavorite = function() {
        var exists = Favorite.addFavorite($scope.mangaId, $scope.mangaDetails.title, decodeParam($routeParams.mangaImage));
        if (exists) {
            $scope.showAddAlertError = true;
            $timeout(function() {
                $scope.showAddAlertError = false;
            }, 5000);
        }
        else {
            $scope.showAddAlert = true;
            $timeout(function() {
                $scope.showAddAlert = false;
            }, 5000);
        }
    };
    

    /** controller logic */


    //check local storage object for manga details with mangaId
    try {
        LocalStorage.getMangaDetailsLocalStorage().exists($scope.mangaId, function(mangaDetailsExists) {
            if (mangaDetailsExists) {
                //it exists, so we can get data from local storage on manga details with mangaId
                LocalStorage.getMangaDetailsLocalStorage().get($scope.mangaId, function(localStorageData) {
                    try {
                        //validate the JSON
                        $scope.mangaDetails = JSON.parse(localStorageData.value);
                        $scope.showDetails = true;
                        Page.setTitle($scope.mangaDetails.title + ' | ' + 'MangaTaku | Read Manga, Add Favorites, Bookmark Pages, and More...');
                    }
                    catch (e) {
                        //console.log(e + " " + e.message);
                        MangaEden.getMangaDetails($scope.mangaId).then(function(data) {
                            if (data) {
                                $scope.mangaDetails = data;
                                //show the details div
                                $scope.showDetails = true;
                                Page.setTitle($scope.mangaDetails.title + ' | ' + 'MangaTaku | Read Manga, Add Favorites, Bookmark Pages, and More...');

                                //save this to manga detail's local storage object
                                LocalStorage.saveMangaDetailsToLocalStorage($scope.mangaId, data);
                            }
                        });
                    }
                    //if it has been longer than 24 hours since last remote call
                    if (TimeUtil.hasExpiredWithinOneDay(localStorageData.lastUpdated)) {
                        //fetch new manga details from remote
                        MangaEden.getMangaDetails($scope.mangaId).then(function(data) {
                            if (data) {
                                $scope.mangaDetails = data;
                                //show the details div
                                $scope.showDetails = true;
                                Page.setTitle($scope.mangaDetails.title + ' | ' + 'MangaTaku | Read Manga, Add Favorites, Bookmark Pages, and More...');
                                //save this to manga detail's local storage object
                                LocalStorage.saveMangaDetailsToLocalStorage($scope.mangaId, data);
                            }
                        });
                    }
                });
            }
            //manga details with mangaId not found in local storage, fetch it from remote
            else {
                MangaEden.getMangaDetails($scope.mangaId).then(function(data) {
                    if (data) {
                        $scope.mangaDetails = data;
                        //show the details div
                        $scope.showDetails = true;
                        Page.setTitle($scope.mangaDetails.title + ' | ' + 'MangaTaku | Read Manga, Add Favorites, Bookmark Pages, and More...');
                        //save this to manga detail's local storage object
                        LocalStorage.saveMangaDetailsToLocalStorage($scope.mangaId, data);
                    }
                });
            }
        });
    }
    catch (e) {
        //console.log(e);
        MangaEden.getMangaDetails($scope.mangaId).then(function(data) {
            if (data) {
                $scope.mangaDetails = data;
                //show the details div
                $scope.showDetails = true;
                Page.setTitle($scope.mangaDetails.title + ' | ' + 'MangaTaku | Read Manga, Add Favorites, Bookmark Pages, and More...');
                //save this to manga detail's local storage object
                LocalStorage.saveMangaDetailsToLocalStorage($scope.mangaId, data);
            }
        });
    }
}
MangaDetailsCtrl.$inject = ['$scope', '$routeParams', '$location', 'MangaEden', 'LocalStorage', 'TimeUtil', 'Favorite', '$timeout', 'Page'];


function MangaChapterCtrl($scope, $location, $exceptionHandler, $routeParams, $window, $timeout, TimeUtil, ImageUtil, MangaEden, LocalStorage, Bookmark, Page) {
    var params = {
        mangaId       : decodeParam($routeParams.mangaId),
        chapterId     : decodeParam($routeParams.chapterId),
        chapterName   : decodeParam($routeParams.chapterName),
        chapterNumber : decodeParam($routeParams.chapterNumber),
        mangaTitle    : decodeParam($routeParams.mangaTitle),
        mangaImage    : decodeParam($routeParams.mangaImage)
    };
    $scope.mangaId = decodeParam($routeParams.mangaId);
    $scope.chapterId = decodeParam($routeParams.chapterId);
    $scope.mangaImage = decodeParam($routeParams.mangaImage);
    $scope.chapterNumber = decodeParam($routeParams.chapterNumber);
    $scope.showAddAlertTop = false;
    $scope.showAddAlertBottom = false;
    $scope.showAddAlertTopError = false;
    $scope.showAddAlertBottomError = false;
 
    Page.setTitle(params.mangaTitle + ' - Chapter ' + params.chapterNumber + ' | ' + 'MangaTaku | Read Manga, Add Favorites, Bookmark Pages, and More...');

    if ($location.search().pageNumber !== "lastPage") {
        $scope.imageNum = parseInt($location.search().pageNumber);
    }
    $scope.addBookmarkTop = function() {
        var exists = Bookmark.addBookmark(params, $scope.imageNum);
        if (exists) {
            $scope.showAddAlertTopError = true;
            $timeout(function() {
                $scope.showAddAlertTopError = false;
            }, 5000);
        }
        else {
            $scope.showAddAlertTop = true;
            $timeout(function() {
                $scope.showAddAlertTop = false;
            }, 5000);
        }
    };
    $scope.addBookmarkBottom = function() {
        var exists = Bookmark.addBookmark(params, $scope.imageNum);
        if (exists) {
            $scope.showAddAlertBottomError = true;
            $timeout(function() {
                $scope.showAddAlertBottomError = false;
            }, 5000);
        }
        else {
            $scope.showAddAlertBottom = true;
            $timeout(function() {
                $scope.showAddAlertBottom = false;
            }, 5000);
        }
    };
    $scope.encodeParam = function(param) {
        return encodeURIComponent(encodeURIComponent(param));
    };

    /** helper functions */

    function getNextChapterId(currentChapterIndex, mangaChapters) {
        //since the manga details for this manga have been loaded, we can proceed to load the next chapter,
        //remember, the array for chapters is in reverse order (highest to lowest)
        var chapterId;
        if (currentChapterIndex > 0) {
            var nextChapterIndex = currentChapterIndex - 1;
            chapterId = mangaChapters[nextChapterIndex][3];
        }
        else {
            //max chapter reached, so we just load chapter 1
            var firstChapterIndex = mangaChapters.length - 1;
            chapterId = mangaChapters[firstChapterIndex][3];
        }
        return chapterId;
    }
    function decodeParam(param) {
        return decodeURIComponent(decodeURIComponent(param));
    }
    function encodeParam(param) {
        return encodeURIComponent(encodeURIComponent(param));
    }
    function getChapterUrl(params, chapterId, chapterName, chapterNumber) {
        return '/mangaChapter/' + encodeParam($scope.mangaId) + '/' + encodeParam(chapterId) +
        '/' + encodeParam(chapterName) + '/' + encodeParam(params.mangaTitle) + '/' +
        encodeParam(params.mangaImage) + '/' + encodeParam(chapterNumber);
    }

    /** controller logic */

    /** retrieve this manga's manga details, so we can do things like look at next/prev chapter */
    //check local storage object for manga details with mangaId
    try {
        LocalStorage.getMangaDetailsLocalStorage().exists($scope.mangaId, function(mangaDetailsExists) {
            if (mangaDetailsExists) {
                //it exists, so we can get data from local storage on manga details with mangaId
                LocalStorage.getMangaDetailsLocalStorage().get($scope.mangaId, function(ls) {
                    //Validate JSON and set the manga list to local storages manga list data
                    try {
                        $scope.mangaDetails = JSON.parse(ls.value);
                        //map the chapters to an array of its numbers so we can find them later
                        $scope.chaptersList = $.map($scope.mangaDetails.chapters, function(e) {
                            return e[0].toString();
                        });
                    }
                    catch (e) {
                        //console.log(e);
                    }
                     /** BEGIN PRELOADING NEXT CHAPTERS IMAGES */
                    //since the que has finished, we preload the next chapters images
                    var currentChapterIndex = $scope.chaptersList.indexOf(params.chapterNumber);
                    var chapterId = getNextChapterId(currentChapterIndex, $scope.mangaDetails.chapters);
                    LocalStorage.getMangaChaptersLocalStorage().exists(chapterId, function(mangaIdExists) {
                        if (mangaIdExists) {
                            //manga chapters exist in local storage
                            LocalStorage.getMangaChaptersLocalStorage().get(params.chapterId, function(ls) {
                                //set the scope images to the image urls in local storage
                                //before we set the view scope images, we push them to an array so the slider works
                                var nextImages = MangaEden.imageListWrapper(JSON.parse(ls.value));
                                ImageUtil.preCacheImages($scope.chapterId, nextImages, "next", $exceptionHandler);
                            });
                        }
                        else {
                            //make a remote call to get the manga chapters images
                            MangaEden.getMangaChapter(chapterId).then(function(data) {
                                var nextImages;
                                if (data) {
                                    //save the chapter images to local storage
                                    LocalStorage.saveMangaChaptersToLocalStorage(chapterId, data);
                                    //before we set the view scope images, we push them to an array so the slider works
                                    nextImages = MangaEden.imageListWrapper(data);
                                }
                                else {
                                    nextImages = ['http://localhost:8888/webapps/manga-tree/app/img/blank.jpg'];
                                }
                                ImageUtil.preCacheImages($scope.chapterId, nextImages, "next", $exceptionHandler);
                            });
                        }
                    });
                    /**END PRELOADING NEXT CHAPTERS IMAGES */
                    //if it has been longer than 24 hours since last remote call
                    if (TimeUtil.hasExpiredWithinOneDay(ls.lastUpdated)) {
                        //fetch new manga details from remote
                        MangaEden.getMangaDetails($scope.mangaId).then(function(data) {
                            if (data) {
                                $scope.mangaDetails = data;
                                //save this to manga detail's local storage object
                                LocalStorage.saveMangaDetailsLocalStorage($scope.mangaId, data);
                                //map the chapters to an array of its numbers so we can find them later
                                $scope.chaptersList = $.map($scope.mangaDetails.chapters, function(e) {
                                    return e[0];
                                });
                            }
                            else {
                                $scope.mangaDetails = {};
                            }
                        });
                    }
                });
            }
            //manga details with mangaId not found in local storage, fetch it from remote
            else {
                MangaEden.getMangaDetails($scope.mangaId).then(function(data) {
                    if (data) {
                        $scope.mangaDetails = data;
                        //save this to manga detail's local storage object
                        LocalStorage.saveMangaDetailsToLocalStorage($scope.mangaId, data);
                        //map the chapters to an array of its numbers so we can find them later
                        $scope.chaptersList = $.map($scope.mangaDetails.chapters, function(e) {
                            return e[0].toString();
                        });
                    }
                    else {
                        $scope.mangaDetails = {};
                    }
                });
            }
        });
    }
    catch (e) {
        //console.log(e);
        //Try to fix local storage by recalling the original manga details
        //****
    }
    

    /** retreive the chapter's images and scope them to the view */
    LocalStorage.getMangaChaptersLocalStorage().exists(params.chapterId, function(mangaIdExists) {
        if (mangaIdExists) {
            //manga chapters exist in local storage
            LocalStorage.getMangaChaptersLocalStorage().get(params.chapterId, function(ls) {
                //if the pageNumber URL query is == lastPage, we find the last page of the chapter
                if ($location.search().pageNumber === "lastPage") {
                    $location.search('pageNumber', JSON.parse(ls.value).length);
                }
                //set the scope images to the image urls in local storage
                //before we set the view scope images, we push them to an array so the slider works
                $scope.images = MangaEden.imageListWrapper(JSON.parse(ls.value)).reverse();
                ImageUtil.preCacheImages($scope.chapterId, $scope.images, "current", $exceptionHandler);
                $scope.image = function() {
                  return $scope.images[$scope.imageNum - 1];
                };
                /** END slider code */
            });
        }
        else {
            //make a remote call to get the manga chapters images
            MangaEden.getMangaChapter(params.chapterId).then(function(data) {
                if (data) {
                    if ($location.search().pageNumber === "lastPage") {
                        $location.search('pageNumber', data.length);
                    }
                    //save the chapter images to local storage
                    LocalStorage.saveMangaChaptersToLocalStorage(params.chapterId, data);
                    //before we set the view scope images, we push them to an array so the slider works
                    $scope.images = MangaEden.imageListWrapper(data).reverse();
                }
                else {
                    $scope.images = ['http://localhost:8888/webapps/manga-tree/app/img/blank.jpg'];
                }
                ImageUtil.preCacheImages($scope.chapterId, $scope.images, "current", $exceptionHandler);
                $scope.image = function() {
                  return $scope.images[$scope.imageNum - 1];
                };
                /** END slider code */
            });
        }
    });

    //load the next chapters images



	/** BEGIN more slider controls */
    $scope.next = function() {
        if ($location.search().pageNumber !== "lastPage") {
            try {
                var a = $scope.images.length;
                 //if it's the last page, the next button should load the next chapter, or go back to chapter 1
                if ($scope.imageNum >= $scope.images.length) {
                    //if the mangaDetails hasn't loaded yet, wait a certain time and call again.
                    if (!$scope.mangaDetails || $scope.mangaDetails.length === 0) {
                        //show loading screen until manga details appears, then load next chapter
                        //!! show and error
                    }
                    else {
                        //get the index of the current chapter, so we can find the next chapter
                        var currentChapterIndex = $scope.chaptersList.indexOf(params.chapterNumber);
                        //since the manga details for this manga have been loaded, we can proceed to load the next chapter,
                        //remember, the array for chapters is in reverse order (highest to lowest)
                        var chapterId;
                        var chapterNum;
                        var chapterName;
                        if (currentChapterIndex > 0) {
                            var nextChapterIndex = currentChapterIndex - 1;
                            chapterId = $scope.mangaDetails.chapters[nextChapterIndex][3];
                            chapterNum = $scope.mangaDetails.chapters[nextChapterIndex][0];
                            chapterName = $scope.mangaDetails.chapters[nextChapterIndex][2];
                            $location.search('pageNumber', 1);
                            $location.path(getChapterUrl(params, chapterId, chapterName, chapterNum));
                        }
                        else {
                            //max chapter reached, so we just load chapter 1
                            var firstChapterIndex = $scope.mangaDetails.chapters.length - 1;
                            chapterId = $scope.mangaDetails.chapters[firstChapterIndex][3];
                            chapterNum = $scope.mangaDetails.chapters[firstChapterIndex][0];
                            chapterName = $scope.mangaDetails.chapters[firstChapterIndex][2];
                            $location.search('pageNumber', 1);
                            $location.path(getChapterUrl(params, chapterId, chapterName, chapterNum));
                        }
                    }
                }
                else {
                    //since it's not the last page, we simply increment the page counter and go to the next page
                    $scope.imageNum++;
                    $location.search('pageNumber', $scope.imageNum);
                }
            }
            catch (e) {
                //console.log(e);
            }
        }
    };
    $scope.prev = function() {
        if ($location.search().pageNumber !== "lastPage") {
            try {
                var a = $scope.images.length;
                if ($scope.imageNum <= 1) {
                    //get the index of the current chapter, so we can find the prev chapter
                    var currentChapterIndex = $scope.chaptersList.indexOf(params.chapterNumber);
                    //if the mangaDetails hasn't loaded yet, wait a certain time and call again.
                    if (!$scope.mangaDetails || $scope.mangaDetails.length === 0) {
                        //show loading screen until manga details appears, then load next chapter
                        //!! show and error
                    }
                    else {
                        var chapterId;
                        var chapterNum;
                        var chapterName;
                        if (currentChapterIndex < $scope.chaptersList.length - 1) {
                            //the first chapter has not been reached, we can just load previous chapter
                            var prevChapterIndex = currentChapterIndex + 1;
                            chapterId = $scope.mangaDetails.chapters[prevChapterIndex][3];
                            chapterNum = $scope.mangaDetails.chapters[prevChapterIndex][0];
                            chapterName = $scope.mangaDetails.chapters[prevChapterIndex][2];
                            $location.search('pageNumber', 'lastPage');
                            $location.path(getChapterUrl(params, chapterId, chapterName, chapterNum));
                        }
                        else {
                            //first chapter reached, so we just load chapter last chapter
                            var lastChapterIndex = 0;
                            chapterId = $scope.mangaDetails.chapters[lastChapterIndex][3];
                            chapterNum = $scope.mangaDetails.chapters[lastChapterIndex][0];
                            chapterName = $scope.mangaDetails.chapters[lastChapterIndex][2];
                            $location.search('pageNumber', 'lastPage');
                            $location.path(getChapterUrl(params, chapterId, chapterName, chapterNum));
                        }
                    }
                }
                else {
                    $scope.imageNum--;
                    $location.search('pageNumber', $scope.imageNum);
                }
            }
            catch (e) {
                //console.log(e);
            }
        }
    };
    /** END more slider controls */
}
MangaChapterCtrl.$inject = ['$scope', '$location', '$exceptionHandler', '$routeParams', '$window', '$timeout', 'TimeUtil', 'ImageUtil', 'MangaEden', 'LocalStorage', 'Bookmark', 'Page'];


function BookmarkCtrl($scope, $routeParams, $timeout, Bookmark, MangaEden, Page) {
    $scope.bookmarks = Bookmark.getBookmarks().reverse();
    $scope.showRemoveAlert = false;
    var removeAlertQueue = [];

    Page.setTitle('Bookmarks | MangaTaku | Read Manga, Add Favorites, Bookmark Pages, and More...');

    $scope.removeBookmark = function(index) {
        Bookmark.removeBookmark(index);
        $scope.showRemoveAlert = true;
        removeAlertQueue.push(index);
        $timeout(function() {
            if (removeAlertQueue.length > 0) {
                removeAlertQueue.pop();
            }
            if (removeAlertQueue.length === 0) {
                $scope.showRemoveAlert = false;
            }
        }, 5000);
    };
    $scope.getMangaImageUrl = function(param) {
        return MangaEden.getBaseImageUrl() + param;
    };
    $scope.chapterDetailsUrl = function(bookmark) {
        return '#!/mangaChapter/' + encodeParam(bookmark.mangaId) + '/' + encodeParam(bookmark.chapterId) + '/' +
        encodeParam(bookmark.chapterName) + '/' + encodeParam(bookmark.mangaTitle) + '/' + encodeParam(bookmark.mangaImage) + '/' +
        encodeParam(bookmark.chapterNumber) + '?pageNumber=' + bookmark.pageNumber;
    };
    function encodeParam(param) {
        return encodeURIComponent(encodeURIComponent(param));
    }
}
BookmarkCtrl.$inject = ['$scope', '$routeParams', '$timeout', 'Bookmark', 'MangaEden', 'Page'];

function FavoritesCtrl($scope, $routeParams, $timeout, Favorite, MangaEden, Page) {
    $scope.favorites = Favorite.getFavorites().reverse();
    //console.log($scope.favorites);
    $scope.showRemoveAlert = false;
    var removeAlertQueue = [];

    Page.setTitle('Favorites | MangaTaku | Read Manga, Add Favorites, Bookmark Pages, and More...');

    $scope.removeFavorite = function(index) {
        Favorite.removeFavorite(index);
        $scope.showRemoveAlert = true;
        removeAlertQueue.push(index);
        $timeout(function() {
            if (removeAlertQueue.length > 0) {
                removeAlertQueue.pop();
            }
            if (removeAlertQueue.length === 0) {
                $scope.showRemoveAlert = false;
            }
        }, 5000);
    };
    $scope.getMangaImageUrl = function(param) {
        return MangaEden.getBaseImageUrl() + param;
    };
    $scope.mangaDetailsUrl = function(favorite) {
        return '#!/mangaDetails/' + encodeParam(favorite.mangaId) + '/' + encodeParam(favorite.mangaImage);
    };
    function encodeParam(param) {
        return encodeURIComponent(encodeURIComponent(param));
    }
}
FavoritesCtrl.$inject = ['$scope', '$routeParams', '$timeout', 'Favorite', 'MangaEden', 'Page'];


function TopTenCtrl($scope, MangaEden, Page) {
    MangaEden.getMangaList("topTen").then(function(fs) {
        try {
            $scope.topTenList = fs.reverse();
        }
        catch (e) {
        }
    });
    $scope.encodeParam = function(param) {
        return encodeURIComponent(encodeURIComponent(param));
    };
    $scope.getMangaImageUrl = function(param) {
        return MangaEden.getBaseImageUrl() + param;
    };
}
TopTenCtrl.$inject = ['$scope', 'MangaEden', 'Page'];