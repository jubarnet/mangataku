'use strict';


// Declare app level module which depends on filters, and services
angular.module('manga-otaku', ['manga-otaku.filters', 'manga-otaku.services', 'manga-otaku.directives']).
	config(['$routeProvider', '$locationProvider', function($routeProvider, $location) {
		$location.hashPrefix('!');
		$routeProvider.when('/', {
			templateUrl: 'partials/home.php',
		});
	    $routeProvider.when('/legal', {
	    	templateUrl: 'partials/legal.html',
	    	controller: LegalCtrl
	    });
	    $routeProvider.when('/mangalist', {
	    	templateUrl: 'partials/mangaList.html',
	    	controller: MangaListCtrl
	    });
	    $routeProvider.when('/favorites', {
	    	templateUrl: 'partials/favorites.html',
	    	controller: FavoritesCtrl
	    });
	    $routeProvider.when('/search/:query', {
	    	templateUrl: 'partials/search.html',
	    	controller: SearchCtrl
	    });
	    $routeProvider.when('/mangaDetails/:mangaId/:mangaImage', {
	    	templateUrl: 'partials/mangaDetails.html', 
	    	controller: MangaDetailsCtrl
	    });
	    $routeProvider.when('/mangaChapter/:mangaId/:chapterId/:chapterName/:mangaTitle/:mangaImage/:chapterNumber', {
	    	templateUrl: 'partials/mangaChapter.html', 
	    	//reloadOnSearch: false,
	    	controller: MangaChapterCtrl
	    });
	    $routeProvider.when('/bookmarks', {
	    	templateUrl: 'partials/bookmarks.html', 
	    	controller: BookmarkCtrl
	    });
	    $routeProvider.otherwise({
	    	redirectTo: '/',
	    	controller: HomeCtrl
	    });
	}]);
