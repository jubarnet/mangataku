'use strict';

/* Directives */

var directives = angular.module('manga-otaku.directives', []);

/**
 * The main search function in the nav bar
 *
 */
directives.directive('autoComplete', [function() {
    return function(scope, iElement, iAttrs) {
        scope.$watch(iAttrs.uiItems, function(values) {
        	var src = $.map(values, function(e) {
        		return e.t;
        	});
            iElement.autocomplete({
            	minLength : 2,
            	delay : 300,
                source: function(request, response) {
                	var results = $.ui.autocomplete.filter(src, request.term);
                	response(results.slice(0, 10));
                },
                select: function() {
                    setTimeout(function() {
                    	iElement.trigger('input');
                    }, 200);
                }
            });
                
        }, true);
    };
}]);

/**
 * This directive is for the main search function in the nav bar
 *
 */
directives.directive('onEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if(event.which === 39 || event.which === 37 || event.keyCode === 39 || event.keyCode === 37) {
                event.stopPropagation();
            }
            if(event.which === 13) {
                setTimeout(function() {
                    scope.$apply(function() {
                        scope.$eval(attrs.onEnter);
                        event.target.value = "";
                        $(element).blur();
                    });
                }, 500);
                event.preventDefault();
            }
        });
    };
});
jQuery(document).on("click", "li.ui-menu-item", function(event) {
    jQuery(".search-query").blur();
    setTimeout(function() {
        jQuery(".perform-search").trigger("click");
    }, 500)
});
jQuery(document).on("keydown", function(event) {
    if (event.which === 37 || event.keyCode === 37) {
        event.stopPropagation();
        jQuery(".prev").trigger("click");
        
    }
    else if (event.which === 39 || event.keyCode === 39) {
        event.stopPropagation();
        jQuery(".next").trigger("click");
    }
});
