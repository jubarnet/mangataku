(function () {
	"use strict";
}());

var services = angular.module("manga-otaku.services", []);

services.factory("TimeUtil", [ function() {
	var timesInMilliSeconds = {
		"now"		: function() { return new Date().getTime(); },
		"oneSecond" : 1000,
		"oneMinute" : 60000,
		"oneHour"   : 3600000,
		"oneDay"    : 86400000,
		"oneWeek"   : 604800000
	};

	var getTimesInMilliSeconds  = function() {
		return timesInMilliSeconds;
	};
	var hasExpiredWithinOneDay  = function(lastUpdated) {
		return timesInMilliSeconds.now() - lastUpdated > getTimesInMilliSeconds().oneDay;
	};
	var hasExpiredWithinOneWeek = function(lastUpdated) {
		return timesInMilliSeconds.now() - lastUpdated > getTimesInMilliSeconds().oneWeek;
	};

	return {
		"getTimesInMilliSeconds"     : getTimesInMilliSeconds,
		"hasExpiredWithinOneDay"    : hasExpiredWithinOneDay,
		"hasExpiredWithinOneWeek"   : hasExpiredWithinOneWeek
	};
}]);

/** LocalStorage returns Local Storage objects with special functions */
services.factory("LocalStorage", [ function() {
	var localStorageKeys = {
		"MangaList"     : "MangaList",
		"MangaDetails"  : "MangaDetails",
		"MangaChapters" : "MangaChapters"
	};

	var mangaList      = new Lawnchair({ "name" : localStorageKeys.MangaList, adapter : "dom" }, function(e) {});
	var mangaDetails   = new Lawnchair({ "name" : localStorageKeys.MangaDetails, adapter : "dom" }, function(e) {});
	var mangaChapters  = new Lawnchair({ "name" : localStorageKeys.MangaChapters, adapter : "dom" }, function(e) {});

	var getLocalStorageKeys = function() {
		return localStorageKeys;
	};
	var getMangaListLocalStorage = function() {
		return mangaList;
	};
	var getMangaDetailsLocalStorage = function() {
		return mangaDetails;
	};
	var getMangaChaptersLocalStorage = function() {
		return mangaChapters;
	};
	var saveMangaListToLocalStorage = function(value) {
		mangaList.save({
			"key"         : localStorageKeys.MangaList,
			"lastUpdated" : new Date().getTime(),
			"value"       : JSON.stringify(value)
		});
	};
	var saveMangaDetailsToLocalStorage = function(mangaId, value) {
		mangaDetails.save({
			"key"         : mangaId,
			"lastUpdated" : new Date().getTime(),
			"value"       : JSON.stringify(value)
		});
	};
	var saveMangaChaptersToLocalStorage = function(chapterId, value) {
		mangaChapters.save({
			"key"         : chapterId,
			"lastUpdated" : new Date().getTime(),
			"value"       : JSON.stringify(value)
		});
	};

	return {
		"getLocalStorageKeys"             : getLocalStorageKeys,
		"getMangaListLocalStorage"        : getMangaListLocalStorage,
		"getMangaDetailsLocalStorage"     : getMangaDetailsLocalStorage,
		"getMangaChaptersLocalStorage"    : getMangaChaptersLocalStorage,
		"saveMangaListToLocalStorage"     : saveMangaListToLocalStorage,
		"saveMangaDetailsToLocalStorage"  : saveMangaDetailsToLocalStorage,
		"saveMangaChaptersToLocalStorage" : saveMangaChaptersToLocalStorage
	};
}]);


services.factory("MangaEden", ["$http", function($http) {
	
	var urls = {
		local : {
			topTenList : "json/topList.json",
			mangaListFileSystem : "json/mangaList.json",
			mangaList : "rest/rest.php?service=mangaeden&type=list",
			mangaDetails : "rest/rest.php?service=mangaeden&type=manga&id=",
			mangaChapter : "rest/rest.php?service=mangaeden&type=chapter&id="
		},
		image : {
			base : "http://cdn.mangaeden.com/mangasimg/"
		}
	};

	var getMangaList = function(from) {
		var req;
		if (from === "local") {
			req = $http.get(urls.local.mangaListFileSystem);
		}
		else if (from === "topTen") {
			req = $http.get(urls.local.topTenList);
		}
		else {
			req = $http.get(urls.local.mangaList);
		}
		return req.then(
			function(response) {
				// success
				if (response && response.status === 200) {
					try {
						//validate JSON
						JSON.parse(JSON.stringify(response.data.manga));
						return response.data.manga;
					}
					catch (e) {
						//console.log(e);
						return false;
					}
					
				}
				else {
					//console.log("Failed to get manga list. Request failed.");
					return false;
				}

			},
			function(response) {
			// error
			//console.log("Failed to call manga list service. Request failed.");
			return false;
		});
	};
	var getMangaDetails = function(mangaId) {
		return $http.get(urls.local.mangaDetails + mangaId).then(
			function(response) {
				// success
				if (response && response.status === 200 && typeof (response.data) === "object") {
					try {
						//validate JSON
						JSON.parse(JSON.stringify(response.data));
						return response.data;
					}
					catch (e) {
						//console.log(e);
						return false;
					}
					
				}
				else {
					//console.log("Failed to get manga details. Request failed.");
					return false;
				}

			},
			function(response) {
			// error
			//console.log("Failed to call manga details service. Request failed.");
			return false;
		});
	};
	var getMangaChapter = function(chapterId) {
		return $http.get(urls.local.mangaChapter + chapterId).then(
			function(response) {
				// success
				if (response && response.status === 200 && typeof (response.data) === "object") {
					try {
						//validate JSON
						
						JSON.parse(JSON.stringify(response.data.images));
						return response.data.images;
					}
					catch (e) {
						//console.log(e);
						return false;
					}
					
				}
				else {
					//console.log("Failed to get manga chapters. Request failed.");
					return false;
				}

			},
			function(response) {
			// error
			//console.log("Failed to call manga chapters service. Request failed.");
			return false;
		});
	};
	var getBaseImageUrl = function() {
		return urls.image.base;
	};
	var imageListWrapper = function(list) {
		var images = [];
		for (var i = 0; i < list.length; i++) {
			images.push(urls.image.base + list[i][1]);
		}
		return images;
	};

	return {
		"getMangaList"     : getMangaList,
		"getMangaDetails"  : getMangaDetails,
		"getMangaChapter"  : getMangaChapter,
		"getBaseImageUrl"  : getBaseImageUrl,
		"imageListWrapper" : imageListWrapper
	};
}]);

services.factory("ImageUtil", ['$timeout', function($timeout) {
	var currentChapterId = null;
	var nextChapterId = null;

	var preCacheImages = function(chapterId, images, type, $exceptionHandler) {
		var proceed;
		if (type === "next" && nextChapterId !== chapterId) {
			nextChapterId = chapterId;
			proceed = true;
		}
		else if (type === "current" && currentChapterId !== chapterId) {
			currentChapterId = chapterId;
			proceed = true;
		}
		else {
			//console.log("IMAGES ALREADY LOADING")
			proceed = false;
		}

		if (proceed) {
			var que = 0;
			var timer = 0;
			angular.forEach(images, function(imageSrc) {
				que++;
				$timeout(function() {
					var image = new Image();
					image.onload = function() {
						que--;
						if (que === 0) {
							//$scope.$broadcast("imagesPreloaded");
							//console.log(type + ' chapter images precached!');
						}
					};
					image.onerror = function() {
						var failedImageSrc = this.src;
						$timeout(function() {
							var image = new Image();
							image.src = failedImageSrc;
						}, 3000);
						$exceptionHandler("There was an error preloading an image.");
					};
					image.src = imageSrc;
				}, timer);
				if (type === "next") {
					timer += 1300;
				}
				else if (type === "current") {
					timer += 600;
				}
			});
		}
	};

	return {
		preCacheImages : preCacheImages
	};
}]);

services.factory("Bookmark", [ function() {

	/** private variables */
	var storage = new Lawnchair({ name : "Bookmarks", adapter : "dom" }, function(e) {});

	var bookmarks = [];

	/** init function */

	(function() {
		try {
			storage.exists("bookmarks", function(exists) {
				exists ? storage.get("bookmarks", function(data) { bookmarks = JSON.parse(data.value); }) : "";
			});
		}
		catch (e) {
			storage.save({ key : "bookmarks", value : JSON.stringify(bookmarks) });
		}
	})();
	
	/** private function */

	var saveBookmarks = function() {
		storage.save({ key : "bookmarks", value : JSON.stringify(bookmarks) });
	};

	/** public functions */

	var addBookmark = function(params, pageNumber) {
		if (typeof (pageNumber) !== "number") {
			return;
		}
		var bookmarkExists = false;
		for (var i = 0; i < bookmarks.length; i++) {
			if (bookmarks[i].mangaId === params.mangaId && bookmarks[i].chapterId === params.chapterId && bookmarks[i].pageNumber === pageNumber) {
				bookmarkExists = true;
			}
		}
		if (!bookmarkExists) {
				var bookmark = {
				"mangaId"		: params.mangaId,
				"chapterId"     : params.chapterId,
				"chapterName"   : params.chapterName,
				"chapterNumber" : params.chapterNumber,
				"mangaTitle"    : params.mangaTitle,
				"mangaImage"    : params.mangaImage,
				"pageNumber"    : pageNumber
			};
			bookmarks.push(bookmark);
			saveBookmarks();
		}
		return bookmarkExists;
	};
	var removeBookmark = function(index) {
		bookmarks.splice(index, 1);
		saveBookmarks();
	};
	var getBookmarks = function() {
		return bookmarks;
	};

	/** api */

	return {
		addBookmark    : addBookmark,
		removeBookmark : removeBookmark,
		getBookmarks   : getBookmarks
	};
}]);

services.factory("Favorite", [ function() {

	/** private variables */
	var storage = new Lawnchair({ name : "Favorites", adapter : "dom" }, function(e) {});

	var favs = [];

	/** init function */

	(function() {
		try {
			storage.exists("favorites", function(exists) {
				exists ? storage.get("favorites", function(data) { favs = JSON.parse(data.value); }) : "";
			});
		}
		catch (e) {
			storage.save({ key : "favorites", value : JSON.stringify(favs) });
		}
		
	})();
	
	/** private function */

	var saveFavorites = function() {
		storage.save({ key : "favorites", value : JSON.stringify(favs) });
	};

	/** public functions */

	var addFavorite = function(mangaId, mangaTitle, mangaImage) {
		//console.log(favs)
		var favoriteExists = false;
		for (var i = 0; i < favs.length; i++) {
			if (favs[i].mangaId === mangaId && favs[i].mangaTitle === mangaTitle && favs[i].mangaImage === mangaImage) {
				favoriteExists = true;
			}
		}
		if (!favoriteExists) {
				var favorite = {
				"mangaId" : mangaId,
				"mangaTitle" : mangaTitle,
				"mangaImage" : mangaImage
			};
			favs.push(favorite);
			saveFavorites();
		}
		return favoriteExists;
	};
	var removeFavorite = function(index) {
		favs.splice(index, 1);
		saveFavorites();
	};
	var getFavorites = function() {
		return favs;
	};

	/** api */

	return {
		addFavorite    : addFavorite,
		removeFavorite : removeFavorite,
		getFavorites   : getFavorites
	};
}]);

services.factory("Page", [ function() {
	var title = "MangaTaku | Read Manga, Add Favorites, Bookmark Pages, and More...";

	var getTitle = function() {
		return title;
	};
	var setTitle = function(newTitle) {
		title = newTitle;
	};

	return {
		"getTitle" : getTitle,
		"setTitle" : setTitle
	}
}]);