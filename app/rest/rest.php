<?php

/*
ADD SECURITY CHECK HERE IN PRODUCTION FOR SURE
*/
// if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1') {
//  	throw new Exception('Invalid remote address.');
// }

class MangaEden {

	private $baseUrl   = 'http://www.mangaeden.com/api';
	private $list      = '/list/0/';
	private $manga     = '/manga/';
	private $chapter   = '/chapter/';

	public function getMangaList() {
		$content = @file_get_contents($this->baseUrl . $this->list);
		if($content === FALSE) {
			echo '';
		}
		else {
			echo $content;
		}
	}

	public function getMangaDetails($mangaId) {
		$content = @file_get_contents($this->baseUrl . $this->manga . $mangaId);
		if($content === FALSE) {
			echo '';
		}
		else {
			echo $content;
		}
	}

	public function getMangaChapters($chapterId) {
		$content = @file_get_contents($this->baseUrl . $this->chapter . $chapterId);
		if($content === FALSE) {
			echo '';
		}
		else {
			echo $content;
		}
	}
}

function main() {
	if ($_GET['service'] == 'mangaeden') {
		$api = new MangaEden();
		if ($_GET['type'] == 'list') {
			$api->getMangaList();
		}
		else if ($_GET['type'] == 'manga') {
			$api->getMangaDetails($_GET['id']);
		}
		else if ($_GET['type'] == 'chapter') {
			$api->getMangaChapters($_GET['id']);
		}
	}
}
main()
?>